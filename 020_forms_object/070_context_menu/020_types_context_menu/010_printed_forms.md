# Печатные формы

Печатная форма - шаблон документа, который может автоматически заполнять поля данными из выбранного бизнес-объекта (также из вложенных бизнес-объектов) для печати.

В режиме администрирования можно создавать неограниченное количество печатных форм в форматах: word и pdf. Имеется возможность настройки прав на просмотр и экспорт.

## Добавление печатной формы в бизнес-объекты:

Для добавления печатной формы в бизнес-объекте нажмите на плюс в верхнем правом углу:

<img width="100%" src="../../030_pics/printedFormsAdd.png">

Из выпадающего списка выберите "Печатные формы":

<img width="100%" src="../../030_pics/listPrintedForms.png">

При выборе с правой стороны появиться лого печатной формы и нажмите на кнопку "Сохранить":

<img width="100%" src="../../030_pics/printerFormsAddedInBO.png">

При нажатии на лого печатной формы откроется окно. Для добавления печатной формы нажмите на кнопку "Добавить документ":

<img width="100%" src="../../030_pics/addMaketPrintedForms.png">

В открывшемся окне заполните обязательные поля и выберите в каком формате документ будет загружаться:

<img width="100%" src="../../030_pics/newWindowPrintedForms.png">

При выборе "Получить пример" загрузиться документ по всем полям которые есть в бизнес-объекте. Их очередность зависит от создания полей в бизнес-объекте:

<img width="100%" src="../../030_pics/responceExampleDoc.png">

В шаблоне можно привести вид как необходимо Вам: поставить стили, размер шрифтов, таблицы.

## Формирование документа для загрузки в систему в виде печатной формы:

Для того чтобы данные в печатной форме заполнялись со значения полей текущего объекта (сущности), необходимо указывать путь к этим полям через формулу: <b>${}</b>

## 1. Для заполнения данных в ПФ с текущего поля в текущем объекте:
## Пример

В печатной форме необходимо отобразить данные с полей “Фамилия”,“Имя” :

<img width="100%" src="../../030_pics/FieldsInAnObject.png">

Указываем в документе имена полей через формулу<b>{Название поля}</b>
${Фамилия}, ${Имя}

<img width="100%" src="../../030_pics/Example.png">

Результат:

<img width="100%" src="../../030_pics/Result.png">

Поля, находящиеся во вкладках, заполняются аналогичным образом.

## 2. Для заполнения данных в ПФ с поля, находящийся, во вложенном объекте:

В печатной форме необходимо отобразить данные с поля “Дата”, находящийся во вложенном объекте “Отчет” :

<img width="100%" width="100%" src="../../030_pics/Fields.png">

<img width="100%" src="../../030_pics/ObjectReport.png">

Указываем в документе имена полей через формулу <b>${Название вложенного объекта_Название поля}</b>
${Отчет_Дата}

<img width="100%" src="../../030_pics/Example2.png">

Результат:

<img width="100%" src="../../030_pics/Result2.png">

При вложенности нескольких объектов, необходимо указать название каждого объекта и название поля:

${Название вложенного объекта1_Название вложенного объекта2_Название вложенного объекта3_Название поля};

<img width="100%" src="../../030_pics/Explanation.png">

В правой вкладке, при загрузке печатной формы, указываются переменные - которые будут заполняться с загружаемого документа. Некорректно указанные поля, отображаются серым цветом.

Пример:

Поля “Дата заполнения заявки”, “Фамилия”, "Имя", Отчество"" корректно сформированы.

Поле “Наименование бизнес-объекта” некорректно сформирован.

<img width="100%" src="../../030_pics/EditingPrintableForm.png">

Далее, нужно сохранить изменения.

## Отображения числа с прописью

Для отображения числа с прописью в печатной форме укажите код: ${Наименование_поля#RUS}

Пример использования: Ваш возраст: ${Ваш возраст} - проставляет числовое значение.

На казахском: ${Ваш возраст#KAZ}

На русском: ${Ваш возраст#RUS}

На английском: ${Ваш возраст#ENG}

На казахском (латиница): ${Ваш возраст#QAZ}

<img width="100%" src="../../030_pics/ageDiffrentLanguage.png">

## Выгрузка готовой печатной формы

<img src="../../030_pics/exampleHowtodownloadPrintedForms.gif">

Чтобы скачать готовый документ, необходимо выйти из режима редактирование нажав на “<img src="../../030_pics/Pencil.png">”,  выбрать объект, где загрузили печатную форму, и в контекстном меню нажать на  
“<img width="43" src="../../030_pics/Addendum.png">”

В списке печатных форм, необходимо выбрать нужную форму и скачать методом клика.

<img width="" src="../../030_pics/Download.png">

## Внесение изменений в печатной форме

Для того чтобы внести изменения в печатной форме необходимо скачать шаблон внести изменения и прикрепить новый файл в систему.

## Удаление печатной формы

Чтобы удалить печатную форму из реестра, необходимо перейти в режим редактирования, нажав на “<img  src="../../030_pics/Pencil.png">“. Выбрать необходимый объект(сущность) и перейти в “Печатные формы” в контекстном меню “<img width="38" src="../../030_pics/Addendum.png">”.

<img width="" src="../../030_pics/Removal.png">

Для удаления печатной формы со списка необходимо выбрать урну.

<img width="100%" src="../../030_pics/Removal2.png">

Далее, нужно сохранить изменения. 

