# Прочти меня

### [Настройка рабочего места программиста](040_for_developers/install/001-00-prepare-workspace.md)

### [Альтернативные запуски DebugServer](040_for_developers/install/run-debug-server/001-run-debug-server.md)

### [Разное](040_for_developers/install/other/000-other.md)

### Ссылки для отладки

- Запуск клиента: [http://localhost:4200](http://localhost:4200)
- Сервер: [http://localhost:1313/web/test/hello](http://localhost:1313/web/test/hello)
- ZooNavigator: [http://localhost:10010](http://localhost:10010)
- KafDrop: [http://localhost:10014](http://localhost:10014)
- Mongo-Express: [http://localhost:10013](http://localhost:10013) - user:admin pass:111
- Elastic: http://localhost:10016

### [In Migration](040_for_developers/install/migration/in/000-in-migration.md)

### [Out Migration](040_for_developers/install/migration/out/000-out-migration.md)

### [Последние работы](040_for_developers/last-works/001-run-alone-kafka-cluster.md)

### [Мониторинг - Grafana dashboards](040_for_developers/install/monitoring/001-overview.md)

### [Переменные окружения](040_for_developers/install/002-01-env-variables.md)

### [API-сервисы](doc/api-service/101-crud.md)

### [Установка и настройка платформы MyBPM (оглавление)](040_for_developers/install/010-platform-MyBPM-toc.md)
