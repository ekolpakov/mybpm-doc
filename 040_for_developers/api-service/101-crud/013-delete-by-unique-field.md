# Удаление инстанции бизнес-объекта (БО) по уникальному полю

```
POST /api/v1/boi/delete-by-unique-field
Content-Type: application/json
```

Входные данные:

```json
{
  "boCode": "Код бизнес-объекта",
  "uniqueFieldCode": "Код уникального поля бизнес-объекта",
  "fieldValue": "Значение уникального поля"
}
```

Ответ: код 200
