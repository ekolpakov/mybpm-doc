# Удаление инстанции бизнес-объекта (БО) по уникальному полю

```
POST /api/v1/boi/delete-by-unique-field-with-id
Content-Type: application/json
```

Входные данные:

```json
{
  "boId": "Идентификатор бизнес-объекта",
  "uniqueFieldId": "Идентификатор уникального поля бизнес-объекта",
  "fieldValue": "Значение уникального поля"
}
```

Ответ: код 200
