# Android Build and uploading to Google Play

## 1. Getting ready
1.1 Firstly, you need to update your git branch ( to get new changes ) and then write
<pre>npm install</pre>
After that, make sure that you prepared everything to **production**.

in environment.ts
<pre>
  ...
  production: true, 
  development: false,
  ...
</pre>

### Skip this step (1.2) if you did not add plugin

>1.2 If you added plugin, you need run command <pre>ionic cap sync android --configuration=production </pre> in terminal
>

1.3 Now run command <pre> ionic cap copy android --configuration=production </pre> in project's terminal

## 2. Android Studio

2.1 Build APK or connect your device and test it. 

2.2 If everything is good, generate signed bundle (.aab)

## 3. Play Google Console

3.1 Open play.google.com and publish your Android App Bundle
