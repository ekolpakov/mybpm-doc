# Рекомендации при работе с elasticsearch

---

## Распределение памяти для elasticsearch

- При распределении памяти, необходимо выделить память для самого контейнера elasticsearch *memlimit*, в зависимости от
  количества выделенной памяти для контейнера, нужно добавить переменную окружения ES_JAVA_OPTS, память, выделенная 
  через данную переменную, будет использована для системного кэша.

  **Важно:** память, которая выдается через ES_JAVA_OPTS, не должна превышать 50% доступной ОЗУ, выделенной на 
  контейнер, потому что вся оставшаяся память будет выделена для
  [Apache Lucene](https://opster.com/guides/elasticsearch/glossary/elasticsearch-lucene/).

  Подробнее можно прочитать 
  [здесь](https://opster.com/guides/elasticsearch/capacity-planning/elasticsearch-memory-usage/).

- В зависимости от потребностей, можно выделить менее 50% памяти, вся оставшаяся память будет выделена для Apache
  Lucene, что может улучшить производительность. Данная практика советуется, если не производятся операции, связанные с
  агрегированием строк *(fielddata)*.

  Подробнее можно прочитать 
  [здесь](https://www.elastic.co/guide/en/elasticsearch/guide/current/heap-sizing.html#_give_less_than_half_your_memory_to_lucene).

- Для лучшей работы *heap size* для elasticsearch не должен превышать
  [30GB](https://www.elastic.co/guide/en/elasticsearch/reference/8.8/advanced-configuration.html#set-jvm-heap-size), 
  так как при использовании меньшей памяти, используются
  [сжатые указатели.](https://www.elastic.co/guide/en/elasticsearch/reference/7.11/advanced-configuration.html#setting-jvm-heap-size)

  Дополнительно можно ознакомиться 
  [здесь](https://www.elastic.co/guide/en/elasticsearch/guide/current/heap-sizing.html#compressed_oops).

- В идеале использовать не больше 4GB heap sizing, так как будут использованы
  [32-битные указатели](https://www.elastic.co/blog/a-heap-of-trouble).

- Выделяемая память для elasticsearch должна быть как можно меньше, не рекомендуется выделять много памяти, так как это
  может привести к более долгим паузам [сборщика мусора](https://www.elastic.co/blog/a-heap-of-trouble).

- При выделении малого количества памяти сборщик мусора не будет освобождать ресурсы в достаточном количестве, что 
  приведет к [переполнению памяти](https://www.elastic.co/blog/a-heap-of-trouble). Также при выполнении запросов, если 
  сервер ноды кластета elasticsearch достигает состояния, при котором происходит нехватка ресурсов, сервер может 
  перестать отвечать,
  [во избежание полного отключения](https://opster.com/guides/elasticsearch/operations/elasticsearch-circuit-breakers/).

---

## Мониторинг сборщика мусора

- Нормальное состояние работы сборщика мусора заключается в частых и периодических срабатываниях освобождения ресурсов в 
  достаточном количестве. Используемая память при работе elasticsearch варьируется от 30% до 70% от 
  общей доступной памяти, выделенной на JAVA процесс ноды, находящейся в кластере elasticsearch,
  [при нормальном состоянии](https://opster.com/guides/elasticsearch/capacity-planning/elasticsearch-heap-size-usage/).

- Нагрузка на память не должна превышать в стабильном состоянии 75%. При высокой потребности нагрузка не должна
  превышать [85%](https://www.elastic.co/blog/found-understanding-memory-pressure-indicator/#green-is-good).

- При нагрузке выше чем
  [85%](https://www.elastic.co/blog/found-understanding-memory-pressure-indicator/#is-high-memory-pressure-always-a-problem)
  стоит провести мониторинг работы сервера elasticsearch, так как это может привести к дальнейшим
  [проблемам](https://www.elastic.co/guide/en/cloud/current/ec-metrics-memory-pressure.html).
  
  Возможные решения можно просмотреть [здесь](https://www.elastic.co/guide/en/cloud/current/ec-memory-pressure.html).

  **Заметка:** чтобы проверить нагрузку на ноды, можно сделать следующий запрос

  ```
  GET {host}/_cat/nodes?v&h=name,node*,heap*
  ```
  
  где *host* - это хост ноды elasticsearch.

---

## Настройка репликации

- При работе состояние кластера должно иметь статус *Green*, это означает, что для каждого основного шарда, количество
  созданных репликационных шардов соответствует настройке количества репликационных шардов для всех индексов. В ином
  случае статус будет
  [желтый или красный](https://www.elastic.co/guide/en/elasticsearch/reference/current/red-yellow-cluster-status.html).

- Количество создаваемых реплик не должно быть больше или равно количеству нод в кластере elasticsearch, так как
  репликационный шард не может создаваться на той же ноде, на которой присутствует основной шард. Подробнее можно
  прочитать [здесь](https://www.elastic.co/guide/en/elasticsearch/reference/current/diagnose-unassigned-shards.html)
  или [здесь](https://www.elastic.co/blog/managing-and-troubleshooting-elasticsearch-memory).

- Настройку количества репликационных шардов можно проверить в настройках индекса по пути:

  ```
  index.number_of_replicas
  ```

---

## Шардирование (Sharding)

- Количество шардов на каждой ноде может быть неограниченным, но для лучшей производительности, на 1GB памяти JVM, иметь
  не более [20 шардов](https://www.elastic.co/blog/managing-and-troubleshooting-elasticsearch-memory).

- Также рекомендуется иметь шарды размером от
  [20GB до 40GB](https://www.elastic.co/guide/en/elasticsearch/reference/current/scalability.html#it-depends). Иметь
  шарды меньшего размера не
  [оптимально](https://www.elastic.co/blog/how-many-shards-should-i-have-in-my-elasticsearch-cluster).

- Также стоит настроить
  [ILM](https://www.elastic.co/guide/en/elasticsearch/reference/current/index-lifecycle-management.html)
  для решения проблемы с большим количеством
  [шардов](https://opster.com/guides/elasticsearch/capacity-planning/elasticsearch-oversharding/).

---
