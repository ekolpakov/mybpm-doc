#!/bin/bash

BOOTSTRAP_SERVERS={BOOTSTRAP_SERVER_ADDRESS:PORT}
TOPICS=$(kafka-topics.sh --list --bootstrap-server ${BOOTSTRAP_SERVERS})

for topic in $TOPICS; do
    topic_size=$(kafka-log-dirs.sh --bootstrap-server "${BOOTSTRAP_SERVERS}" --topic-list "${topic}" --describe | grep '^{' | jq '[ ..|.size? | numbers ] | add')
    echo "Topic '$topic' size: $topic_size B"
done
