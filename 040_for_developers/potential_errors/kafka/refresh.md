# Кейс с топиком REFRESH

## Вводная часть
<p>
    Здесь описан проблемный кейс с топиком REFRESH. Его "симптомы", причины и способ его решения.
</p>

### Симптомы
    - Не работает KafDrop
    - В логах pod-а Кафки бесконечно крутится ошибка NotEnoughReplicasException

### Причина возникновения. 
<p>
    При подобной топологии перед рестартом любой из инстанции Кафки, 
    необходимо полностью остановить отправку сообщений В Кафку и считывание ИЗ Кафки - отключить pod-ы Java. 
    Во время обновления кафки было нарушено это правило, что привело к дестабилизации топика Refresh. 
</p>

### Способ решения 
<p>
    Было принято решение удалить соответствующий топик (не рекомендуется!). Удаление было осуществлено при помощи Kafka CLI команды.
    После удаления топика возникла проблема - топик не создавался заново при рестарте системы. Из LiquidMongo была удалена запись
    для того чтобы система заново создала топик. Однако это привело к другой ошибке, в связи с чем была добавлена новая переменная 
    окружения - MYBPM_TOPIC_SUFFIX_REFRESH. Используя эту переменную благополучно был создан топик REFRESH1.
</p>
