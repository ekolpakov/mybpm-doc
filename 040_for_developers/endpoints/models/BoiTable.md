# Описание модели данных BoiTable

Хранит таблицу инстанций для конкретной страницы

* **[List](../Footnote#List)<[BoiTableHead](./BoiTableHead)>** `heads`

    Список отображаемых полей инстанции

* **[List](../Footnote#List)<[BoiTableRecord](./BoiTableRecord)>** `records`

    Список инстанций

* **String** `businessObjectId`

    Идентификатор Бизнес объекта, инстанции которого перечисленны в этой таблице

* **boolean** `hasNext`

    Флаг, показывающий наличие ещё записей после этой страницы

* **long** `totalHits`

    Описание общего количества найденных записей

* **boolean** `totalHitsMore`

    Флаг, указывающий, что найдено записей больше, чем totalHits

* **long** `tookInMillis`

    Сколько времени в миллисекундах занял поиск этой страницы
