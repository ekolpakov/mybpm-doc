# Описание точек входа контроллера BoiController

Методы:

* [_loadBracketBoiTable_](#-loadbracketboitable)

## Метод loadBracketBoiTable

Используется для загрузки реестра инстанций для выбранного бизнес объекта с переданными фильтрами.

### `POST` `/web/business-object-instance/load-bo-instance-bracket-table`

#### Заголовки:

| Ключ  | Тип    | Описание                                                                     |
|:------|:-------|:-----------------------------------------------------------------------------|
| token | String | Токен авторизации пользователя (получается [_так_](./AuthController#-login)) |

#### Параметры:

Значения всех параметров должны быть в **JSON** формате и **URLEncoded**

| Параметр      | Тип                                                      |
|:--------------|:---------------------------------------------------------|
| requestFilter | [_BracketRequestFilter_](../models/BracketRequestFilter) |

#### Ответ:

В качестве ответа возвращается [BoiTable](../models/BoiTable) в **JSON** формате
