#!/bin/bash

search_dir='./'
output_dir='pdf_output'

rm -rf "$output_dir"

# Get all .md files
for entry in $(find $search_dir -maxdepth 5 -type f -name '*.md' | sort); do
  input="${entry#$search_dir}"

  # Skip main readme
  if [[ "$input" == "000"* ]] || [ "$input" == "" ]; then
    continue
  fi

  filename="${input%.*}"
  outputFileName="$output_dir/$filename.pdf"

  echo "Generate pdf for '$filename' and save into '$outputFileName'"

  mkdir -p "$outputFileName" && rm -rf "$outputFileName"

  pandoc --latex-engine=xelatex -V mainfont="Ubuntu Mono" \
  -V colorlinks=true \
  -V linkcolor=blue \
  -V urlcolor=red \
  -V toccolor=gray \
  "$input" -o "$outputFileName"
done

cd "$output_dir" || exit

echo ""
echo "Add all pdf into archive"
echo ""

# Get all .pdf files
for entry in $(find "./" -maxdepth 5 -type f -name '*.pdf' | sort); do
  zip -qur "endpoints.zip" "$entry"
done

echo "******************************"
echo "*                            *"
echo "*           DONE             *"
echo "*                            *"
echo "******************************"
