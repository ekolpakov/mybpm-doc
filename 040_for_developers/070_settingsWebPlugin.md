# Настройка клиентского плагина MyBPM

1. Подключаем нужные библиотеки из репозиториев <b>"jsrepo.greetgo"</b>
2. Подготавливаем нужные компоненты плагина
3. Добавляем компоненты, модули в <b>webpack.config.js</b> и <b>docker/lib/plugins-config.json</b>
    1. Структура <b>"plugins-config.json"</b>
    2. <b>"pluginProjectName"</b> название проекта плагина, uniqueName из webpack.config.js
    3. <b>"path"</b> путь до plugin.js
    4. <b>"plugins"</b> массив плагинов находящихся в этом проекте
    5. <b>"plugins.key"</b> уникальный айди плагина
    6. <b>"plugins.type"</b> тип плагина
    7. <b>"plugins.exposedModule"</b> название модуля которое ранее указали в <b>webpack.config.js</b>
    8. <b>"plugins.ngModuleName"</b> имя компонента или модуля в плагине
    9. <b>"plugins.route"</b> используется при type === 'PAGE', роутинг плагина
5. Общие зависимости нужно прописать в <b>webpack.config.js</b>
6. Запускаем скрипт <b>"plugin-config-copy-to-debug.bash"</b> после чего запускаем <b>mybpm-web</b>
7. Проверяем подключился ли плагин
8. Коммит, пуш

---

Доступные типы плагинов, на данный момент:

<p><b>CONSOLE</b> - Текст в консоль</p>
<p><b>MENU_ITEM</b> - Добавление меню в список</p>
<p><b>PAGE</b> - Отдельная страница</p>
<p><b>OVERLAY</b> - Оверлей на главный экран</p>
<p><b>IN_OUT</b> - Взаимодействие между ядром и плагином</p>
<p><b>COMPANY_PAGE_HEADER_CONTENT</b> - Контент в шапку страницы компании</p>
<p><b>BO_VIEWER_HEADER_CONTENT</b> - Контент в шапку реестра бо</p>
<p><b>REGISTER_PAGE_BOTTOM_CONTENT</b> - Контент под кнопку в регистрации</p>
<p><b>AUTH_ADDITIONAL_LOGO</b> - Внешний логотип на странице авторизации</p>
<p><b>HEADER_ADDITIONAL_LOGO</b> - Внешний логотип в шапке системы</p>
