# 1) Установка MyBPM на кластере Kubernetes.

Перед установкой необходимо получить доступы к следующим сервисам:

- Apache Zookeeper
- Apache Kafka
- MongoDB
- ElasticSearch
- PostgreSQL
- Кластер Kubernetes

## 1.1) Образы платформы

Предварительно нужно получить актуальные на данный момент версии платформы - это имена образов docker. На данный момент
актуальными являются следующие образы:

- out.mybpm.kz/mybpm-api-dev:4.5.1215 - API-сервис платформы MyBPM
- out.mybpm.kz/mybpm-web-dev:4.5.1111 - Web-представление платформы MyBPM

В вашем случае имена образов могут поменяться. Не забудьте в дальнейшем заменять эти названия на ваши.

## 1.2) Подготовка пространства имён

В этом руководстве используется пространство имён:

    mybpm-platform-test

У вас оно должно быть другим и отражать суть кластера, например:

    mybpm-prod-stone

Которое может обозначать платформу MyBPM на рабочей среде в проекте Stone - выберите понятное вам название.
В дальнейшем в этом руководстве везде заменяйте mybpm-platform-test на ваше.

Создать можно это пространство имён выполнив команду:

    kubectl apply -f kube-yaml-files/010-prepare/10-ns.yaml

## 1.3) Подготовка секретного ключа для загрузки образа из репозитория

Образы платформы MyBPM загружаются из репозитория dockerhub.mybpm.kz и доступ к нему должен быть предоставлен.
Вам предоставят файл, с ключом доступа. Пример такого файла находиться здесь:

    kube-yaml-files/020-MyBPM-in-kube/05-auth-out-mybpm-kz.json

В файле:

    kube-yaml-files/020-MyBPM-in-kube/06-create-docker-secret.bash

Находиться скрипт, чтобы его применить. В нём указано пространство имен: mybpm-platform-test - поменяйте его на ваше
и потом запустите его:

    bash kube-yaml-files/020-MyBPM-in-kube/06-create-docker-secret.bash

Данный скрипт создаст в Kubernetes объект

    secret/out-mybpm-kz

Через который Kubernetes будет обращаться к репозиторию out.mybpm.kz для скачивания образов. Этот kube-объект
в дальнейшем будет указываться в yaml-файлах.

## 1.4) Распределение по хостам кластера Kubernetes

Для того чтобы контролировать, на каких хостах кластера Kubernetes будут запускаться сервисы платформы MyBPM нужно
создать соответствующие метки на хостах, и потом запускаемые контейнеры привязать к этим меткам.

Для настройки соответствующих меток имеется скрипт:

    bash kube-yaml-files/020-MyBPM-in-kube/07-distribute-node-labels.bash

В нём есть текст:

    setup_node master1 +api -web
    setup_node master2 +api -web
    setup_node master3 -api -web
    setup_node worker4 -api -web
    setup_node worker5 -api +web
    setup_node worker6 -api +web

Который добавляет метку api=ok на хосты master1 и master2, а метку web=ok на хосты: worker5 и worker6.
В вашем случае может быть по другому - откорректируйте данный кусок скрипта, как вам нужно.
Теперь его нужно запустить.

Проверить распределение меток можно с помощью команды:

    kubectl get node --show-labels

## 1.5) API-сервис платформы MyBPM

Теперь нужно развернуть API-сервис платформы MyBPM
