# Fedora 35: установка дополнительного ПО

Команда установит базовые программы необходимые программисты:

    sudo dnf install -y java-11-openjdk.x86_64 java-11-openjdk-src.x86_64 jq

Что здесь установилось:

- java-11-openjdk.x86_64 - это основной пакет, где содержится язык java для разработчика
- java-11-openjdk-src.x86_64 - в этом пакете устанавливаются исходные коды ядра языка java
- jq - консольная утилита для обработки данным в формате json

Ещё необходимо установить docker-compose для этого надо запустить следующие команды:

    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname 
    -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
