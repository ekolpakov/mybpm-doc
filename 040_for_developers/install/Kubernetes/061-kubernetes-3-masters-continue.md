# Установка kubernetes с тремя мастерами и "плавающим" IP-мастера (продолжение)

Начало [здесь](060-kubernetes-3-masters.md)

Структура кластера:

| IP-сервера     | Назначение         | Название хоста в ssh |
|----------------|--------------------|----------------------|
| 192.168.17.137 | плавающий IP-адрес |                      |
| 192.168.17.131 | первый мастер      | master1              |
| 192.168.17.132 | второй мастер      | master2              |
| 192.168.17.133 | третий мастер      | master3              |
| 192.168.17.134 | первый рабочий     | worker4              |
| 192.168.17.135 | второй рабочий     | worker5              |
| 192.168.17.136 | третий рабочий     | worker6              |

### 5) Окончательная подготовка серверов

#### 5.1) Настройка доменных имён

Необходимо, чтобы сервера будущего кластера могли обращаться друг к другу не только по IP-адресам,
но и по доменам, равным их именам хостов (hostname). Для этого необходимо прописать домены в файле /etc/hosts на всех серверах следующим образом:

    192.168.17.130 master0
    192.168.17.131 master1
    192.168.17.132 master2
    192.168.17.133 master3
    192.168.17.134 worker4
    192.168.17.135 worker5
    192.168.17.136 worker6

Пройдитесь по всем серверам и внесите эти строки в конец файла /etc/hosts - текущее содержание файла удалять нельзя.

Должно получиться что-то типа:

    user@master1:~$ cat /etc/hosts
    127.0.0.1 localhost
    
    # The following lines are desirable for IPv6 capable hosts
    ::1     ip6-localhost ip6-loopback
    fe00::0 ip6-localnet
    ff00::0 ip6-mcastprefix
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters
    
    192.168.17.130 master0
    192.168.17.131 master1
    192.168.17.132 master2
    192.168.17.133 master3
    192.168.17.134 worker4
    192.168.17.135 worker5
    192.168.17.136 worker6

На всех серверах (мастерах и worker-ах).

#### 5.2) Настройка плавающего IP-адреса - keepalived

Плавающий IP-адрес необходимо настраивать ТОЛЬКО на мастерах.

##### 5.2.1) Устанавливаем keepalived

На всех мастерах устанавливаем программу:

    sudo apt install -y keepalived

##### 5.2.2) Настраиваем конфиг keepalived

На всех мастерах нужно создать конфиг

    cat | sudo tee /etc/keepalived/keepalived.conf > /dev/null

С содержанием:

    global_defs {
      enable_script_security
    }
    
    vrrp_script myhealth {
      script "/bin/nc -z -w 2 127.0.0.1 22"
      interval 10
      user nobody
    }
    
    vrrp_instance VI_1 {
      state MASTER
      interface enp0s3
      virtual_router_id 230
      priority 100
      advert_int 1
      nopreempt
      notify /etc/keepalived/keepalived-notify.sh root
      authentication {
        auth_type PASS
        auth_pass UmvGvLwCZV
      }
      virtual_ipaddress {
        192.168.17.137/16 dev enp0s3 label enp0s3:0
      }
      track_script {
        myhealth
      }
    }

(Ctrl+D - чтобы записать файл и выйти)

<b>ВНИМАНИЕ!</b> в этом файле поменяйте IP адрес с маской подсети: 192.168.17.137/16 - на тот который используется у вас.

<b>ВНИМАНИЕ!</b> в этом файле поменяйте интерфейс enp0s3 на тот, который используется у вас

Этот файл на разных мастерах будет немного отличаться. Ниже в таблице указаны отличия

| Сервер, на котором нужно поменять | Найти строку | Заменить её на |
|-----------------------------------|--------------|----------------|
| master1                           | MASTER       | MASTER         |
| master1                           | priority 100 | priority 100   |
| master2                           | MASTER       | BACKUP         |
| master2                           | priority 100 | priority 99    |
| master3                           | MASTER       | BACKUP         |
| master3                           | priority 100 | priority 98    |

##### 5.2.3) Настраиваем файл оповещений

На всех мастерах нужно создать файл:

    cat | sudo tee /etc/keepalived/keepalived-notify.sh > /dev/null

С содержанием:

    #!/bin/sh
    umask -S u=rwx,g=rx,o=rx
    exec echo "[$(date -Iseconds)]" "$0" "$@" >>"/var/run/keepalived.$1.$2.state"

(Ctrl+D - чтобы записать файл и выйти)

И обеспечить его запуск:

    sudo chmod +x /etc/keepalived/keepalived-notify.sh

Перезапустить сервис:

    sudo systemctl restart keepalived

#### 5.2) Установка cilium - ТОЛЬКО НА ОДНОМ МАСТЕРЕ: master1

Cilium - это сетевая инфраструктура Kubernetes. Её установка описана на странице:

    https://docs.cilium.io/en/stable/gettingstarted/k8s-install-default/

В разделе "Install the Cilium CLI". Нужно выполнить команды от туда - здесь они повторены. Выполните:

    CILIUM_CLI_VERSION=$(curl -s https://raw.githubusercontent.com/cilium/cilium-cli/master/stable.txt)
    CLI_ARCH=amd64
    if [ "$(uname -m)" = "aarch64" ]; then CLI_ARCH=arm64; fi
    curl -L --fail --remote-name-all https://github.com/cilium/cilium-cli/releases/download/${CILIUM_CLI_VERSION}/
    cilium-linux-${CLI_ARCH}.tar.gz{,.sha256sum}
    sha256sum --check cilium-linux-${CLI_ARCH}.tar.gz.sha256sum
    sudo tar xzvfC cilium-linux-${CLI_ARCH}.tar.gz /usr/local/bin
    rm cilium-linux-${CLI_ARCH}.tar.gz{,.sha256sum}

Далее проверьте версию установленного клиента, выполнив команду:

    cilium version

Должно вылететь что-то типа:

    cilium-cli: v0.13.1 compiled with go1.20 on linux/amd64
    cilium image (default): v1.13.0
    cilium image (stable): v1.13.0
    cilium image (running): unknown. Unable to obtain cilium version, no cilium pods found in namespace 
    "kube-system"

### 6) Инициация кластера kubernetes на первом мастере

#### 6.1) Инициализируем первый мастер

Теперь настало время инициировать кластер kubernetes на первом мастере. Заходим на первый мастер:

    ssh master1

Создаём файл (в домашней директории):

    cat > init-kube.sh

С содержимым:

    #!/bin/bash
    sudo kubeadm init --upload-certs --control-plane-endpoint=192.168.17.137 --node-name master1 
    --pod-network-cidr=10.244.0.0/16

(Ctrl+D - чтобы записать файл и выйти)

Здесь используется плавающий IP-адрес мастеров, и сейчас он должен быть у первого мастера. Если у вас другой плавающий
адрес подкорректируйте его.

Далее делаем файл исполняемым:

    chmod +x init-kube.sh

И запускаем его:

    ./init-kube.sh

Данная команда скачает основные образы кластера из интернета и запустит их, в конце должна вылететь информация
похожая на это:

    Your Kubernetes control-plane has initialized successfully!
    
    To start using your cluster, you need to run the following as a regular user:
    
        mkdir -p $HOME/.kube
        sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
        sudo chown $(id -u):$(id -g) $HOME/.kube/config
    
    Alternatively, if you are the root user, you can run:
    
    export KUBECONFIG=/etc/kubernetes/admin.conf
    
    You should now deploy a pod network to the cluster.
    Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
    https://kubernetes.io/docs/concepts/cluster-administration/addons/
    
    You can now join any number of the control-plane node running the following command on each as root:
    
    kubeadm join 192.168.17.137:6443 --token ivge50.w197dx3p2dtfb84g \
        --discovery-token-ca-cert-hash sha256:48a40932495e2ca93b5042fce15e0109141dab2b40ef0056ff3a62dc7eeb83ee \
        --control-plane --certificate-key 981b3e7f4f5ef40cc15359b2802a5369ee56228acc02e1c9ac2e86061ed94516
    
    Please note that the certificate-key gives access to cluster sensitive data, keep it secret!
    As a safeguard, uploaded-certs will be deleted in two hours; If necessary, you can use
    "kubeadm init phase upload-certs --upload-certs" to reload certs afterward.
    
    Then you can join any number of worker nodes by running the following on each as root:
    
    kubeadm join 192.168.17.137:6443 --token ivge50.w197dx3p2dtfb84g \
        --discovery-token-ca-cert-hash sha256:48a40932495e2ca93b5042fce15e0109141dab2b40ef0056ff3a62dc7eeb83ee 

Данную информацию необходимо сохранить в файл 

    cat > result-init.txt

Далее выделите в терминале выше эту информацию и вставьте в эту команду,
и нажмите Ctrl+D, чтобы записать её в файл и выйти.

После этого нужно настроить команду kubectl для этого нужно в терминале запустить команды:

     mkdir -p $HOME/.kube
     sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
     sudo chown $(id -u):$(id -g) $HOME/.kube/config

#### 6.2) Инициализируем сеть в кластере

Далее необходимо установить сеть в кластере, для этого нужно создать файл:

    cat > cilium_install.bash

И вставить в него содержимое:

    #!/bin/bash
    cilium install --encryption ipsec

(Ctrl+D - чтобы записать файл и выйти)

Сделать файл запускаемым:

    chmod +x cilium_install.bash

И запустить его:

    ./cilium_install.bash

После можно посмотреть результат установки кластера:

    kubectl get pod --all-namespaces

Должно вылететь что-то типа такого:

    NAMESPACE     NAME                               READY   STATUS    RESTARTS      AGE
    kube-system   cilium-99nwc                       1/1     Running   0             75m
    kube-system   cilium-operator-864b7b486c-v8bxm   1/1     Running   1 (65m ago)   105m
    kube-system   cilium-rkpjg                       1/1     Running   0             64m
    kube-system   coredns-787d4945fb-q8shx           1/1     Running   0             106m
    kube-system   etcd-master3                       1/1     Running   0             65m
    kube-system   kube-apiserver-master1             1/1     Running   1             107m
    kube-system   kube-controller-manager-master1    1/1     Running   2 (65m ago)   107m
    kube-system   kube-proxy-56fbm                   1/1     Running   0             75m
    kube-system   kube-scheduler-master1             1/1     Running   3 (65m ago)   107m

Везде статус должен быть Running - если где-то он не такой, то нужно подождать несколько минут - должны докачаться
файлы из интернета и установиться.

#### 6.3) Инициализация остальных мастеров и worker-ов

После инициализации первого мастера необходимо инициализировать второй мастер, выполнив на нём следующие команды:

    ssh master2
    
    kubeadm join 192.168.17.137:6443 --token ivge50.w197dx3p2dtfb84g \
        --discovery-token-ca-cert-hash sha256:48a40932495e2ca93b5042fce15e0109141dab2b40ef0056ff3a62dc7eeb83ee \
        --control-plane --certificate-key 981b3e7f4f5ef40cc15359b2802a5369ee56228acc02e1c9ac2e86061ed94516

    exit

Третий мастер:

    ssh master3
    
    kubeadm join 192.168.17.137:6443 --token ivge50.w197dx3p2dtfb84g \
        --discovery-token-ca-cert-hash sha256:48a40932495e2ca93b5042fce15e0109141dab2b40ef0056ff3a62dc7eeb83ee \
        --control-plane --certificate-key 981b3e7f4f5ef40cc15359b2802a5369ee56228acc02e1c9ac2e86061ed94516

    exit

Также нужно подключить к кластеру все worker-ы:

    ssh workerN

    kubeadm join 192.168.17.137:6443 --token ivge50.w197dx3p2dtfb84g \
        --discovery-token-ca-cert-hash sha256:48a40932495e2ca93b5042fce15e0109141dab2b40ef0056ff3a62dc7eeb83ee

И так для всех worker-ов.

<b>ВНИМАНИЕ!</b> Команды для подключения нужно брать из файла result-init.txt, который вы создали ранее

После этого можно посмотреть какие ноды подключены к кластеру, выполнив команду:

    kubectl get node

Должно вылететь что-то типа:

    NAME      STATUS   ROLES           AGE    VERSION
    master1   Ready    control-plane   115m   v1.26.2
    master2   Ready    control-plane   73m    v1.26.2
    master3   Ready    control-plane   74m    v1.26.2
    worker4   Ready    <none>          84m    v1.26.2
    worker5   Ready    <none>          84m    v1.26.2
    worker6   Ready    <none>          84m    v1.26.2

Все ноды должны иметь статус Ready, что обозначает, что они готовы к работе

Ну и команда:

    kubectl get pod --all-namespaces -o wide

Должна выводить все ноды:

    NAMESPACE   NAME                             READY STATUS  RESTARTS    AGE  IP             NODE    NOMINATED NODE READINESS GATES
    kube-system cilium-99nwc                     1/1   Running 0           89m  192.168.17.134 worker4 <none>         <none>
    kube-system cilium-hj4nl                     1/1   Running 0           89m  192.168.17.135 worker5 <none>         <none>
    kube-system cilium-n2fbk                     1/1   Running 0           89m  192.168.17.136 worker6 <none>         <none>
    kube-system cilium-operator-864b7b486c-v8bxm 1/1   Running 1 (79m ago) 119m 192.168.17.131 master1 <none>         <none>
    kube-system cilium-psrx9                     1/1   Running 0           119m 192.168.17.131 master1 <none>         <none>
    kube-system cilium-r7rnl                     1/1   Running 0           79m  192.168.17.133 master3 <none>         <none>
    kube-system cilium-rkpjg                     1/1   Running 0           78m  192.168.17.132 master2 <none>         <none>
    kube-system coredns-787d4945fb-j86bc         1/1   Running 0           121m 10.0.0.42      master1 <none>         <none>
    kube-system coredns-787d4945fb-q8shx         1/1   Running 0           121m 10.0.0.91      master1 <none>         <none>
    kube-system etcd-master1                     1/1   Running 1           121m 192.168.17.131 master1 <none>         <none>
    kube-system etcd-master2                     1/1   Running 0           79m  192.168.17.132 master2 <none>         <none>
    kube-system etcd-master3                     1/1   Running 0           79m  192.168.17.133 master3 <none>         <none>
    kube-system kube-apiserver-master1           1/1   Running 1           121m 192.168.17.131 master1 <none>         <none>
    kube-system kube-apiserver-master2           1/1   Running 1 (79m ago) 79m  192.168.17.132 master2 <none>         <none>
    kube-system kube-apiserver-master3           1/1   Running 1 (79m ago) 79m  192.168.17.133 master3 <none>         <none>
    kube-system kube-controller-manager-master1  1/1   Running 2 (79m ago) 121m 192.168.17.131 master1 <none>         <none>
    kube-system kube-controller-manager-master2  1/1   Running 0           78m  192.168.17.132 master2 <none>         <none>
    kube-system kube-controller-manager-master3  1/1   Running 0           77m  192.168.17.133 master3 <none>         <none>
    kube-system kube-proxy-56fbm                 1/1   Running 0           89m  192.168.17.134 worker4 <none>         <none>
    kube-system kube-proxy-9qx9k                 1/1   Running 0           121m 192.168.17.131 master1 <none>         <none>
    kube-system kube-proxy-9ttjl                 1/1   Running 0           79m  192.168.17.133 master3 <none>         <none>
    kube-system kube-proxy-k48m5                 1/1   Running 0           78m  192.168.17.132 master2 <none>         <none>
    kube-system kube-proxy-r4b76                 1/1   Running 0           89m  192.168.17.136 worker6 <none>         <none>
    kube-system kube-proxy-wm2qb                 1/1   Running 0           89m  192.168.17.135 worker5 <none>         <none>
    kube-system kube-scheduler-master1           1/1   Running 3 (79m ago) 121m 192.168.17.131 master1 <none>         <none>
    kube-system kube-scheduler-master2           1/1   Running 0           78m  192.168.17.132 master2 <none>         <none>
    kube-system kube-scheduler-master3           1/1   Running 0           79m  192.168.17.133 master3 <none>         <none>

#### 6.4) Необходимо открыть мастера для запуска на них рабочих подов

Для этого необходимо создать файл:

    cat > masters-allow-pods.bash

И вставить в него содержимое:

    #!/bin/bash
    kubectl taint nodes --all node-role.kubernetes.io/control-plane-

Сделать этот файл исполняемым:

    chmod +x masters-allow-pods.bash

И запустить его:

    ./masters-allow-pods.bash

Далее смотрите [Развёртка платформы MyBPM на кластере kubernetes](070-MyBPM.md)
