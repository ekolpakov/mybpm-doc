# Ubuntu 20.04 : настройка docker

Создаём конфигурационную директорию

```bash
sudo mkdir /etc/docker
```

И настраиваем докер

```bash
cat | sudo tee /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
"storage-driver": "overlay2",
"storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF
```

Теперь нужно у докера отрубить memlock, для этого открываем файл:

```bash
sudo mcedit -b /usr/lib/systemd/system/docker.service
```

Если нет файла: /usr/lib/systemd/system/docker.service То его место можно найти по команде:

```bash
systemctl status docker
```

В открытом файле находим строку:

```bash
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
```

И добавляем в её конец строку:

```bash
--default-ulimit memlock=-1:-1
```

Чтобы получилась строка:

```bash
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock --default-ulimit memlock=-1:-1
```

Перезапускаем докер:

```bash
sudo systemctl daemon-reload
sudo systemctl restart docker
```

Также, чтобы не использовать sudo для команды docker, надо добавить пользователя в группу docker

```bash
sudo usermod -aG docker $USER
```

Проверить отключение лимитов на память можно командой:

```bash
docker run --rm busybox:1.28 cat /proc/1/limits
```

Должна появиться строка

```
...
Max locked memory         unlimited            unlimited            bytes     
...
```

Эти ограничения должны быть unlimited (т.е. ограничений нет)
