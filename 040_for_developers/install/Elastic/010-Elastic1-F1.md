# 4) Установка эластика по топологии Elastic1-F1

Кластер будет разворачиваться мимо kubernetes - как отдельный сервис, управляемый подсистемой systemd.

## 4.1) Предварительные требование

Необходимо иметь сервер, на который будем устанавливать кластер эластика.

Необходимо иметь ssh-доступ к этому серверу. В нашем случае доступ к серверу будет осуществляться с помощью команды:

    ssh master1

И предполагается что у сервера имеется IP-адрес:

    192.168.17.131

У вас могут быть другие значения - учитывайте это в дальнейшем.

## 4.2) Установка OpenJDK

Zookeeper написан на языке программирования Java и поэтому он нуждается в JVM-е, и её нужно установить.
Для этого запустите команды:

    ssh master1 sudo apt install -y openjdk-17-jre

## 4.3) Скачивание и проверка дистрибутивов

Также зайдите на своём компьютере в директорию dist

    cd dist

Если её нет, то создайте. И скачайте последнюю версию программы со страницы:

    https://www.elastic.co/downloads/elasticsearch

Для Linux x86_64

Например, такими командами:

    wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.6.2-linux-x86_64.tar.gz
    wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.6.2-linux-x86_64.tar.gz.sha512

Проверяем скаченный дистрибутив на отсутствие ошибок скачивания:

    sha512sum -c elasticsearch-8.6.2-linux-x86_64.tar.gz.sha512

## 4.4) Копирование дистрибутивов на сервера

Предполагается, что на серверах уже есть директория dist в домашней директории пользователя. Если нет, то создайте:

    ssh master1 mkdir dist

Дальше нужно скопировать скаченный архив на сервера, где он будет устанавливаться:

    scp elasticsearch-8.6.2-linux-x86_64.tar.gz master1:dist/

Теперь его нужно установить. Далее будет описано как установить его на один сервер. Вам нужно будут это сделать для всех
серверов, кластера Elastic Search.

## 4.5) Установка на один сервер

Заходим на сервер и в папку dist:

    ssh master1
    cd dist

Все программы в системах Unix, которые устанавливаются вручную, устанавливаются в директорию /opt.
(Ubuntu является Linux-ом, а Linux является Unix-ом)

Убедитесь, что она существует. Если нет, то создайте её.

    sudo mkdir -p /opt/

Вы находитесь в директории ~/dist. Распакуйте дистрибутив:

    tar xvf elasticsearch-8.6.2-linux-x86_64.tar.gz

Появиться директория:

    elasticsearch-8.6.2

Перенесите её в opt:

    sudo mv ./elasticsearch-8.6.2 /opt/

Далее перейдём в папку /opt

    cd /opt

Создаём пользователя, из под которого будет работать сервер:

    sudo useradd elastic

Поменяйте владельца файлов на root и поправьте некоторые права:

    sudo chown -Rv root:elastic ./elasticsearch-8.6.2
    sudo chmod g+w ./elasticsearch-8.6.2/config

Делаем символическую ссылку (чтобы в последствии было проще обновлять версию)

    sudo ln -s elasticsearch-8.6.2 elastic

Теперь переходим в папку kafka

    cd /opt/elastic/

Создаём директорию, где будут храниться данные и логи:

    sudo mkdir -p /data/elastic/data
    sudo mkdir -p /data/elastic/logs

У вас эта директория может оказаться где-то в другом месте. Если так, то в дальнейшем не забудьте менять эту директорию
на нужную вам.

Передадим их в собственность пользователя kafka:

    sudo chown -Rv elastic: /data/elastic

Перенаправим логи:

    sudo rmdir logs
    sudo ln -s /data/elastic/logs logs

Теперь нужно настроить конфиг Elastic Search:

    sudo mcedit -b config/elasticsearch.yml

И меняем его текст на следующий:

    cluster.name: mybpm-cluster
    node.name: node-1
    path.data: /data/elastic/data
    path.logs: /data/elastic/logs
    bootstrap.memory_lock: true
    network.host: 192.168.17.131
    http.port: 9200
    readiness.port: 9399
    xpack.security.enabled: false
    #action.destructive_requires_name: false

Ещё нужно настроить память для Java, для этого редактируем файл:

    sudo mcedit -b config/jvm.options

В открывшемся файле находим строки, где задаётся -Xms и -Xmx и устанавливаем там нужное значение например:

    -Xms4g
    -Xmx4g

Проследите, чтобы перед ними не было знака решётка (#) - если он есть, удалите.

Нужно для пользователя elastic прописать разрешения - откройте файл:

    sudo mcedit -b /etc/security/limits.d/elastic.conf

И вставьте туда текст:

    elastic soft memlock unlimited
    elastic hard memlock unlimited

Теперь временно расширьте параметр:

    echo 262144 | sudo tee /proc/sys/vm/max_map_count

И зафиксируем его - откройте файл:

    sudo mcedit -b /etc/sysctl.d/20-elastic.conf

И внесите в него текст:

    vm.max_map_count = 262144

Активируем настройку:

    sudo sysctl -p

Сделаем пробный запуск сервера Elastic Search выполнив команду:

    sudo -u elastic bin/elasticsearch

Далее посыпятся логи - логи должны остановиться и сервер остаться запущенным. Если так, то всё ок.
Иначе нужно исправлять ошибки

Ctrl+C и сервер остановиться.

## 4.6) Настройка systemd

Теперь нужно настроить сервис в операционной системе. Для этого создайте файл:

    sudo mcedit -b /etc/systemd/system/elastic.service

И вставьте в него текст:


    [Unit]
    Description=Elastic Search
    Requires=network.target
    After=syslog.target network.target remote-fs.target nss-lookup.target
    
    [Service]
    User=elastic
    Group=elastic
    LimitMEMLOCK=infinity
    StandardOutput=syslog
    StandardError=syslog
    WorkingDirectory=/opt/elastic
    ExecStart=/opt/elastic/bin/elasticsearch
    ExecReload=/bin/kill -s HUP $MAINPID
    ExecStop=/bin/kill -s -TERM $MAINPID
    TimeoutStopSec=30
    # When a JVM receives a SIGTERM signal it exits with code 143
    SuccessExitStatus=143 0
    Type=simple
    Restart=on-failure
    RestartSec=10
    PIDFile=/data/elastic/elastic.pid
    
    MemoryHigh=4000M
    MemoryMax=4000M
    
    [Install]
    WantedBy=multi-user.target

После этого запустите:

    sudo systemctl daemon-reload

И активируйте новый сервис:

    sudo systemctl enable elastic

Ну а теперь запустите сам сервер как сервис в операционной системе:

    sudo systemctl start elastic

Посмотреть статус сервиса можно командой:

    sudo systemctl status elastic

Должно вылететь что-то типа:

    ● elastic.service - Elastic Search
     Loaded: loaded (/etc/systemd/system/elastic.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2023-03-16 07:43:01 UTC; 7min ago
    Main PID: 399189 (java)
    Tasks: 73 (limit: 9402)
    Memory: 2.5G (high: 3.9G max: 3.9G available: 1.3G)
    CPU: 34.047s
    CGroup: /system.slice/elastic.service
    ├─399189 /opt/elastic/jdk/bin/java -Xms4m -Xmx64m -XX:+UseSerialGC -Dcli.name=server 
      -Dcli.script=/opt/elastic/bin/elasticsearch -Dcli.libs=lib/tools/server-cli -Des.path.home=/opt/elastic 
      -Des.path.conf=/opt/elastic/config -Des.distribution.type=tar -cp "/opt/elastic/lib/*:/opt/elastic/lib/
       cli-launcher/*" org.elasticsearch.launcher.CliToolLauncher
    ├─399254 /opt/elasticsearch-8.6.2/jdk/bin/java -Des.networkaddress.cache.ttl=60 
      -Des.networkaddress.cache.negative.ttl=10 -Djava.security.manager=allow -XX:+AlwaysPreTouch -Xss1m 
      -Djava.awt.headless=true -Dfile.encoding=UTF-8 -Djna.nosys=true -XX:-OmitStackTraceInFastThrow 
      -Dio.netty.noUnsafe=true -Dio.netty.noKeySetOptimization=true -Dio.netty.recycler.maxCapacityPerThread=0 
      -Dlog4j.shutdownHookEnabled=false -Dlog4j2.disable.jmx=true -Dlog4j2.formatMsgNoLookups=true 
      -Djava.locale.providers=SPI,COMPAT --add-opens=java.base/java.io=ALL-UNNAMED -Xms2g -Xmx2g -XX:+UseG1GC 
      -Djava.io.tmpdir=/tmp/elasticsearch-1937012648588012979 -XX:+HeapDumpOnOutOfMemoryError 
      -XX:+ExitOnOutOfMemoryError -XX:HeapDumpPath=data -XX:ErrorFile=logs/hs_err_pid%p.log 
      "-Xlog:gc*,gc+age=trace,safepoint:file=logs/gc.log:utctime,pid,tags:filecount=32,filesize=64m" 
      -XX:MaxDirectMemorySize=1073741824 -XX:G1HeapRegionSize=4m -XX:InitiatingHeapOccupancyPercent=30 
      -XX:G1ReservePercent=15 -Des.distribution.type=tar --module-path /opt/elasticsearch-8.6.2/lib 
      --add-modules=jdk.net -m org.elasticsearch.server/org.elasticsearch.bootstrap.Elasticsearch
    └─399297 /opt/elastic/modules/x-pack-ml/platform/linux-x86_64/bin/controller

