# Как настроить OUT миграцию для старта

## Самое начало

    kz/greetgo/mybpm/model_kafka_mongo/mongo/bo/field/BoFieldDto.java

Нужно проставить needUploadToOutTable=true в поля, которые должны быть мигрированы

## Генерация файла с DDL для просмотра таблиц, которые попадут в out миграцию

    MigrationController

## Чтобы миграция запускалась нужно в конфиге

    /mybpm/configs/OutMigrationPostgresConfig.txt

Поставить true у параметра hasOutMigration

## Дополнительно можно проставить в env yaml файле значение как в примере ниже для: 
1) управления схемой, куда будут мигрировать данные с системы 
2) управлением OutMigrationConsumer GroupId

    `- { name: MYBPM_OUT_MIGRATION_VERSION, value: "1" }`

## В отличие от in миграции, out работает с помощью кафки, и ловит изменения

    KafkaBoi

## Конфиги нужные для миграции:

    1. kz/greetgo/mybpm/register/config/out_migration/OutMigrationDataSourceConfig.java
    2. kz/greetgo/mybpm/register/config/out_migration/OutMigrationPostgresConfig.java

## Классы, где смотреть код:

    1. kz/greetgo/mybpm/register/consumer/controller/out_migration/OutMigrationConsumer.java
    2. kz/greetgo/mybpm/register/impl/migration/out/OutMigrationImpl.java
    3. kz/greetgo/mybpm/register/impl/migration/out/OutMigrationWorkerImpl.java
