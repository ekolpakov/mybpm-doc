# 2) Установка Zookeeper с топологией Zoo1

Кластер будет разворачиваться мимо kubernetes - как отдельные сервисы, управляемым подсистемой systemd.

## 2.1) Предварительные требование

Необходимо иметь один сервер, на который будем устанавливать зукипер.

Необходимо иметь ssh-доступ к этому серверу. В нашем случае доступ к серверу будет осуществляться с помощью команды:

    ssh zoo1

В вашем случае это может быть иначе, учтите это в дальнейшем.

## 2.2) Установка OpenJDK

Зукипер написан на языке программирования Java и поэтому он нуждается в JVM-е, и её нужно установить.
Для этого запустите команды:

    ssh zoo1 sudo apt install -y openjdk-17-jre

## 2.3) Скачивание и проверка дистрибутивов

Также зайдите на своём компьютере в директорию dist

    cd dist

Если её нет, то создайте. И скачайте последнюю версию программы со страницы:

    https://dlcdn.apache.org/zookeeper/zookeeper-3.8.1/

Выполнив команды:

    wget https://dlcdn.apache.org/zookeeper/zookeeper-3.8.1/apache-zookeeper-3.8.1-bin.tar.gz
    wget https://dlcdn.apache.org/zookeeper/zookeeper-3.8.1/apache-zookeeper-3.8.1-bin.tar.gz.sha512

После нужно проверить правильно ли загрузился файл, выполнив команду:

    sha512sum -c apache-zookeeper-3.8.1-bin.tar.gz.sha512

Должно вылететь:

    apache-zookeeper-3.8.1-bin.tar.gz: OK

Что файл загрузился без ошибок.

## 2.4) Копирование дистрибутивов на сервера

Предполагается, что на сервере уже есть директория dist в домашней директории пользователя. Если нет, то создайте:

    ssh zoo1 mkdir dist

Дальше нужно скопировать скаченный архив на сервера, где он будет устанавливаться:

    ssh scp apache-zookeeper-3.8.1-bin.tar.gz zoo1:dist/

Теперь его нужно установить. Далее будет описано как установить его на один сервер. Вам нужно будут это сделать для всех
серверов, кластера Zookeeper.

## 2.5) Установка на сервер

Заходим на сервер и в папку dist:

    ssh zoo1
    cd dist

Все программы в системах Unix, которые устанавливаются вручную, устанавливаются в директорию /opt.
(Ubuntu является Linux-ом, а Linux является Unix-ом)

### 2.5.1) Подготовка директории /opt/zookeeper/

Убедитесь, что она существует. Если нет, то создайте её.

    sudo mkdir -p /opt/

Вы находитесь в директории ~/dist. Распакуйте дистрибутив:

    tar xvf apache-zookeeper-3.8.1-bin.tar.gz

Появиться директория:

    apache-zookeeper-3.8.1-bin

Перенесите её в opt:

    sudo mv ./apache-zookeeper-3.8.1-bin /opt/

Далее перейдём в папку /opt

    cd /opt

Меняем владельца файлов на root:

    sudo chown -Rv root: ./apache-zookeeper-3.8.1-bin

Делаем символическую ссылку (чтобы в последствии было проще обновлять версию)

    sudo ln -s ./apache-zookeeper-3.8.1-bin zookeeper

### 2.5.2) Настройка конфигурации и директории для данных

Теперь переходим в папку zookeeper

    cd /opt/zookeeper/

Создаём пользователя, из под которого будет работать сервер:

    sudo useradd zookeeper

Создаём директорию, где будут храниться данные:

    sudo mkdir -p /data/zookeeper

У вас эта директория может оказаться где-то в другом месте. Если так, то в дальнейшем не забудьте менять эту директорию
на нужную вам.

Создадим директорию для логирования:

    sudo mkdir -p /data/zookeeper/logs

Передадим их в собственность пользователя zookeeper:

    sudo chown -Rv zookeeper: /data/zookeeper

Мы находимся в директории /opt/zookeeper - сделаем символическую ссылку на директорию логирования:

    sudo ln -s /data/zookeeper/logs logs

Теперь настроим конфиг:

    sudo mcedit -b conf/zoo.cfg

И вставим туда текст:

    tickTime = 2000
    dataDir=/data/zookeeper
    clientPort = 2181
    initLimit = 5
    syncLimit = 2

Обратите внимание на строку dataDir=/data/zookeeper - поставьте здесь нужный вам путь.

### 2.5.3) Пробный запуск сервера

Теперь можно сделать пробный запуск сервера:

    sudo -u zookeeper bin/zkServer.sh start

Если есть ошибки - исправить их. Если всё нормально - остановить сервер:

    sudo -u zookeeper bin/zkServer.sh stop

Проверьте папку /data/zookeeper - там должна появиться директория version-2.

### 2.5.4) Настройка системного сервиса systemd

Теперь нужно настроить автоматический запуск с помощью systemd. Для этого создайте файл:

    sudo mcedit -b /etc/systemd/system/zookeeper.service

И вставьте в него текст:

    [Unit]
    Description=Zookeeper Daemon
    Documentation=http://zookeeper.apache.org
    Requires=network.target
    After=network.target
    
    [Service]    
    Type=forking
    WorkingDirectory=/opt/zookeeper
    User=zookeeper
    Group=zookeeper
    ExecStart=/opt/zookeeper/bin/zkServer.sh start /opt/zookeeper/conf/zoo.cfg
    ExecStop=/opt/zookeeper/bin/zkServer.sh stop /opt/zookeeper/conf/zoo.cfg
    ExecReload=/opt/zookeeper/bin/zkServer.sh restart /opt/zookeeper/conf/zoo.cfg
    TimeoutSec=30
    Restart=on-failure
    
    MemoryHigh=500M
    MemoryMax=500M
    
    [Install]
    WantedBy=default.target

После этого запустите:

    sudo systemctl daemon-reload

И активируйте новый сервис:

    sudo systemctl enable zookeeper

Ну а теперь запустите сам сервер как сервис в операционной системе:

    sudo systemctl start zookeeper

Посмотреть статус сервиса можно командой:

    sudo systemctl status zookeeper

Должно вылететь что-то типа:

    ● zookeeper.service - Zookeeper Daemon
     Loaded: loaded (/etc/systemd/system/zookeeper.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2023-03-15 03:46:56 UTC; 1h 53min ago
       Docs: http://zookeeper.apache.org
    Main PID: 273020 (java)
    Tasks: 49 (limit: 9402)
    Memory: 56.2M (high: 500.0M max: 500.0M available: 443.7M)
    CPU: 9.983s
    CGroup: /system.slice/zookeeper.service
    └─273020 java -Dzookeeper.log.dir=/opt/zookeeper/bin/../logs -Dzookeeper.log.file=zookeeper-zookeeper
    -server-worker6.log
    ......................
    om.sun.management.jmxremote -Dcom.sun.management.jmxremote.local.only=false org.apache.zookeeper.server.
    quorum.QuorumPeerMain /opt/zookeeper/conf/zoo.cfg
    
    Mar 15 03:46:55 worker6 systemd[1]: Starting Zookeeper Daemon...
    Mar 15 03:46:55 worker6 zkServer.sh[273005]: /usr/bin/java
    Mar 15 03:46:55 worker6 zkServer.sh[273005]: ZooKeeper JMX enabled by default
    Mar 15 03:46:55 worker6 zkServer.sh[273005]: Using config: /opt/zookeeper/conf/zoo.cfg
    Mar 15 03:46:56 worker6 zkServer.sh[273005]: Starting zookeeper ... STARTED
    Mar 15 03:46:56 worker6 systemd[1]: Started Zookeeper Daemon.

Выйдите туда где есть yaml-файлы и запустите команду:

    kubectl apply -f yaml-files/030-Zookeeper-Kafka/10-zoo-navigator.yaml

В kubernetes запуститься zoo-navigator и его можно посмотреть в браузере по адресу:

    http://192.168.17.137:30302

Здесь указан плавающий IP-адрес.

