# Тестирование нагрузки ElasticSearch

Класс для запуска BigElasticProbe (kz.greetgo.mybpm.register.probes.elastic.performance_test.BigElasticProbe).

Настройки находятся в классе InputParameters (kz.greetgo.mybpm.register.probes.elastic.performance_test.InputParameters)

Также в PRG_ROOT/build/big-elastic-probe - создаются файлы управления:

    Loading Data/Start.btn - удалив этот файл запускается     один поток загрузки данных в индекс эластика
    Loading Data/Stop.btn  - удалив этот файл останавливается один поток загрузки данных в индекс эластика
    Loading Data/Stop All.btn  - удалив этот файл останавливаются все потоки загрузки данных в индекс эластика
    Loading Data/Show.btn  - удалив этот файл показвается текущее состояние загрузки

