# OUT Migration Kafka пример

### Пример как пройсходит миграция поле

#### Самое начало

---

Термин: БО - Бизнес Объект.

---

Допустим в системе есть БО Клиент с полями ИИН, ИМЯ, ФАМИЛИЯ, ДОКУМЕНТ. <br />
ИИН - уникальное поле,ДОКУМЕНТ является вложенным БО. <br />
Поля БО Документ - НОМЕР, ДАТА. НОМЕР - уникальное поле.

По внешней миграций хотим получить все события (создание, обновление, удаление инстанций БО и тд.)

Структура БО Клиент

<img src="../010_pics/outMigrationBoClientExample.png" width="100%" alt="">


Структура БО Документ

<img src="../010_pics/outMigrationBoDocumentExample.png" width="100%" alt="">

---

Для того чтобы миграция началась надо настройть внешнюю миграцию.

Для этого переходим в раздел Настройки в главном меню.

<img src="../010_pics/myBpmSettings.png" width="50%" alt="">

Далее кликаем на Настройки

<img src="../010_pics/settings.png" width="30%" alt="">

Видим раздел OUT Миграция кликаем.

Кликнув видим пустую настройку.

<img src="../010_pics/outMigrationNotEnabled.png" width="100%" alt="">

Нажимаем на чекбокс, чтобы активировать настройку.

<img src="../010_pics/outMigrationBoSettingsList.png" width="100%" alt="">

Видим два БО Клиент и Документ.

Раскрыв БО-ты можем указать поля, которые будут участвовать в миграции

<img src="../010_pics/outMigrationBoFieldSettings.png" width="100%" alt="">

Дале выбираем поля. Примечание: поля с красными звёздочками - это уникальные поля, они нужны для миграций вложенных БО.

<img src="../010_pics/outMigrationBoFieldSelectedSettings.png" width="100%" alt="">

Переходим в "Настройки подключение".

<img src="../010_pics/outMigrationConnectionSettings.png" width="100%" alt="">

Здесь необходимо указать название топика, в который будут записываться данные.

<img src="../010_pics/outMigrationConnectionTopic.png" width="100%" alt="">

Сохраняем. </br>

Прекрасно, мы всё настроили.

---

## Давайте создадим инстанции БО клиент.

Для этого переходим в реестр БО клиент и нажимаем на кнопку добавить.

<img src="../010_pics/outMigrationClientAddInstance.png" width="100%" alt="">

Заполняем поля.

<img src="../010_pics/outMigrationClientInstanceFiled.png" width="100%" alt="">

Сохраняем и видим в реестре есть запись.

<img src="../010_pics/outMigrationClientInstance.png" width="100%" alt="">

---

## Результат, в топике OUT_MIGRATION появилась запись формата JSON.


    {
        "recordId":null,
        "externalId":null,
        "id":"yKjoIid~C@R1h2Nf",
        "boCode":"client",
        "fields":[
                {
                "code":"lastname",
                "apiValue":"Testtest",
                "updatedAt":1692865330851
                },
                {
                "code":"name",
                "apiValue":"Test",
                "updatedAt":1692865330851
                },
                {
                "code":"iin",
                "apiValue":"1101010101010",
                "updatedAt":1692865330851
                }
            ],
        "boFields":[
            {
            "fieldCode":"doc",
            "toBoCode":null,
            "toFieldCode":"number",
            "apiValue":"1",
            "updatedAt":1692865330851
                }
        ],
        "state":"ACTUAL"
    }
