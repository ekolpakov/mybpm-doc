# Шаблон отчетов

## Пример создания шаблона отчетов

### Создание Бизнес Объекта

Для того, чтобы создать шаблон, нам потребуется Бизнес Объект (БО). Создаем (или используем существующий) БО.

В данном примере мы создаем БО "Человек" с нижепоказанными полями:

<img src="../010_pics/report-template-1.png" alt="report-template-1" width="100%">

### Создание шаблона

Чтобы создать шаблон отчетов, находим и нажимаем на <b>"Аналитику"</b>

<img src="../010_pics/report-template-2.png" alt="report-template-2" width="100%">

После того, как нажали на <b>Аналитику</b>, Вы должны увидеть примерно следующую картину:

<img src="../010_pics/report-template-3.png" alt="report-template-3" width="100%">

Нажимаем на кнопку <b>"Добавить"</b>:

<img src="../010_pics/report-template-4.png" alt="report-template-4" width="100%">

Выбираем <b>"Отчеты"</b>:

<img src="../010_pics/report-template-5.png" alt="report-template-5" width="100%">

В открытой вкладке <b>"Общая информация"</b> мы должны увидеть следующую картину:

<img src="../010_pics/report-template-6.png" alt="report-template-6" width="100%">

Заполняем <b>"Название отчета"</b> (обязательное) и <b>"Описание"</b> (необязательное)

<img src="../010_pics/report-template-7.png" alt="report-template-7" width="100%">

Нажимаем на вкладку (слева) <b>"Вид отчета"</b> и увидим пустую колонну:

<img src="../010_pics/report-template-8.png" alt="report-template-8" width="100%">

Нажимаем на <b>"Объект"</b> и выбираем нужную БО (в нашем случае БО "Человек")

<img src="../010_pics/report-template-9.png" alt="report-template-9" width="100%">

Нажимаем на <b>"Атрибут"</b> и выбираем нужное поле (в нашем случае "Имя")

<img src="../010_pics/report-template-10.png" alt="report-template-10" width="100%">

Нажимаем на <b>"Итого"</b> и выбираем, что надо показывать в самом конце этой колонны (в нашем случае мы хотим вывести
общее количество элементов)

<img src="../010_pics/report-template-11.png" alt="report-template-11" width="100%">

Заполняем поле <b>"Подпись итого"</b>

<img src="../010_pics/report-template-12.png" alt="report-template-12" width="100%">

Заполняем <b>"Название столбца"</b>

<img src="../010_pics/report-template-13.png" alt="report-template-13" width="100%">

Нажимаем на кнопку <b>"Добавить"</b> и создаем следующую колонну

<img src="../010_pics/report-template-14.png" alt="report-template-14" width="100%">

Таким же образом заполняем создаем и заполняем 2 колонны (Фамилия и Дата рождения)

<img src="../010_pics/report-template-15.png" alt="report-template-15" width="100%">

Нажимаем на кнопку <b>"Сохранить"</b>

<img src="../010_pics/report-template-16.png" alt="report-template-16" width="100%">

После успешного сохранения мы увидим шаблон отчета, которого только что создали

<img src="../010_pics/report-template-17.png" alt="report-template-17" width="100%">
