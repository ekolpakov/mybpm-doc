# IN Migration Kafka примеры

### Пример мигрирование поле

---

Термин: Бизнес-объект - БО

В дальнейшем уроке буду использовать данный термин для обозначения Бизнес-объекта

---

Давайте создадим бизнес-объект Клиент с полями ИИН, ИМЯ, ФАМИЛИЯ. ИИН - уникальное поле.

Для начало надо нажать на карандашик которая находиться в правом верхнем углу (серый карандаш).

<img src="../010_pics/businessModeAdmin.png" width="100%" alt="businessModeAdmin">

После нажатия карандаш становиться синим, это означает что вы находитесь в режиме редактирования, данном режиме можно создать бизнес объект

<img src="../010_pics/pencilBlue.png" width="25px" alt="pencilBlue">

Убедившись что карандаш стал синим, нажимаем на зелёный крест, который добавляет новый бизнес объекты и прочее

<img src="../010_pics/adminModeAddPlusSign.png" width="100%" alt="adminModeAddPlusSign">

Появиться окошка и нажимаем на Бизнес-объект

<img src="../010_pics/adminModeAddBO.png" width="100%" alt="adminModeAddBO">

Появиться пустой БО

<img src="../010_pics/emptyBo.png" width="100%" alt="emptyBo">

Давайте переименуем название БО на Клиент-а, для этого надо нажать на Наименование 

<img src="../010_pics/emptyClientBo.png" width="100%" alt="emptyClientBo">

Осталось добавить поля как мы обговаривали ИИН, ИМЯ, ФАМИЛИЯ давайте добавим. В Элементах-страницы находиться поля Число, двойным нажатием на данное поле оно зарегистрирует в БО.

<img src="../010_pics/numberField.png" width="100%" alt="numberField">

Результат: 

<img src="../010_pics/numberFieldInBo.png" width="100%" alt="numberFieldInBo">

Надо переименовать название добавленного поле на ИИН и сохранить.

<img src="../010_pics/iinField.png" width="100%" alt="iinField">

Теперь добавим 2 тестовых поле для Имени и Фамилий.

<img src="../010_pics/twoTextField.png" width="100%" alt="twoTextField">

Надо переименовать название добавленных полей на ИМЯ, ФАМИЛИЯ и сохранить.

<img src="../010_pics/nameAndSurnameField.png" width="100%" alt="nameAndSurnameField">

Сохраняем изменения нажатием на кнопку СОХРАНИТЬ.

<img src="../010_pics/boWithInnNameSurname.png" width="100%" alt="boWithInnNameSurname">

После надо изменить код БО, и полей. 

С начало изменим код самого БО, для этого надо нажать на шестиугольник которая находиться в правом верхнем углу.

<img src="../010_pics/shestiugolnik.png" width="100%" alt="shestiugolnik">

Проставляем код который мы хотим, допустим будет "Client".

<img src="../010_pics/shestiugolnikSave.png" width="100%" alt="shestiugolnikSave">

Загорелась зеленая кнопка СОХРАНИТЬ и нажимаем на него.

На этом моменте мы изменили код у БО на "Client".

Осталось изменить коды полей данного БО, у каждого поле есть свой настройки находиться правее у поля.

Нажимаем на шестерёнку.

<img src="../010_pics/iinFieldSettings.png" width="100%" alt="iinFieldSettings">

После нажатия должна появиться окошка настроек.

<img src="../010_pics/fieldSettings.png" width="100%" alt="fieldSettings">

Далее надо кликнуть изменить код.

<img src="../010_pics/fieldSettingsChangeCodeIIn.png" width="392" alt="fieldSettingsChangeCode">

Код данного поле "IIN" такой код поля нас вполне устраивает.

Точно также надо проделать с остальными полями.

Давайте поменяем код у поля ИМЯ, для этого также нажимаем на шестерёнку у плоя ИМЯ затем кликаем на "Изменить код".

<img src="../010_pics/fieldSettingsChangeCode2.png" width="392" alt="fieldSettingsChangeCode2">

Код данного поле "IMYa" такой код поля нас не устраивает, меняем на "NAME"".

<img src="../010_pics/fieldSettingChangeCodeChanged1.png" width="392" alt="fieldSettingChangeCodeChanged1">

Изменили теперь сохраняем.

Осталось только поле ФАМИЛИЯ давайте приступим.

Для этого точно так же как и у остальных полей нажимаем на шестерёнку у поля ФАМИЛИЯ, далее кликаем изменить код.

<img src="../010_pics/fieldCodeSettingsChangeCode3.png" width="392" alt="fieldCodeSettingsChangeCode3">

Код данного поле "FAMILIYA" такой код поля нас не устраивает, меняем на "LASTNAME".

<img src="../010_pics/fieldSettingChangeCodeChanged2.png" width="392" alt="fieldSettingChangeCodeChanged2">

После того как изменили код на "LASTNAME" нажимаем сохранить.

Поздравляю мы полностью подготовили БО Клиент для миграций через кафку

---

## Давайте сформируем JSON для БО Клиент.

Как мы уже знаем,

У БО Клиент код равен "Client",

Код поля ИИН равен "IIN",

Код поля ИМЯ равен "NAME",

Код поля ФАМИЛИЯ равен "LASTNAME".



      в recordId     передаем рандомное значение.
      в externalId   передаем иднетификатор из вашей системы, если таковы имеются.
      в boCode       передаем код нашего БО которое мы определили "Client".

      в fields       имеет 2 значения "code", "apiValue".
                     в code      передаем код поле ИМЯ, как мы уже определили код поле равен "NAME"
                     в apiValue  передаем имя "Сергей".
                     Так как fields это массив передаем код и значения для полей ФАМИЛИЯ и ИИН,
                     как описано выше.
      в state        можем прописать такие значения как "ACTUAL","REMOVED", "ARCHIVED", "TEST".

### Формируем JSON.

         {"recordId": "random-value",
         "externalId": "bfsseVCjGC225bM@",
         "boCode": "Clinet",
         "fields": [
            {
             "code": "NAME",
             "apiValue": "Сергей"
            },
            {
               "code" : "LASTNAME",
               "apiValue" : "Иванов"
            },
            {
               "code" : "IIN",
               "apiValue" : "17"
            }
         ],
          "state": "ACTUAL"}
---


### Надо создать топик для кафки

Отправляем сформированный JSON в топик которую вы создали


### Осталось сохранить ваш топик в настройках MyBPM.


### Как сохранить топики в настройках MyBPM?

1) В меню кликнуть на **Настройки**.

<img src="../010_pics/myBpmSettings.png" alt="kafka_topic_list">

2) Далее появиться окошка настроек.

<img src="../010_pics/settingMenuItem.png" alt="kafka_topic_list">

3) Кликнуть на IN Миграция.

<img src="../010_pics/kafkaInMigrationTopic.png" width="100%" alt="kafka_topic_list">

4) Заполнить поле ***Список топиков*** топиками которые вы уже за ранее создали.

<img src="../010_pics/inMigrationKafkaSettingWithTopic.png" width="100%" alt="kafka_topic_list">

5) После заполнение нажмите сохранить.

#### Система уже подписалась на ваши топики и готов мигрировать

---


### Результат, появиться новая запись Клиента с ИИН 17, ИМЯ Сергей, ФАМИЛИЯ Иванов.

<img src="../010_pics/clientInstance.png"  width="100%" alt="kafka_topic_list">
